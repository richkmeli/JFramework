package it.richkmeli.jframework.crypto.algorithm.bouncycastle.pqc.crypto.gmss;

import it.richkmeli.jframework.crypto.algorithm.bouncycastle.crypto.Digest;

public interface GMSSDigestProvider {
    Digest get();
}
