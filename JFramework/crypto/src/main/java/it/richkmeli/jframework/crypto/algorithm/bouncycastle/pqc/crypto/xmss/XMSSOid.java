package it.richkmeli.jframework.crypto.algorithm.bouncycastle.pqc.crypto.xmss;

public interface XMSSOid {

    int getOid();

    String toString();
}
